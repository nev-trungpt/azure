#REF: https://blog.frankel.ch/dockerfile-maven-based-github-projects/

#Try to use multiple stage build to see the difference


# TestJava1-Docker container-CI
FROM maven:3.5-jdk-8-alpine AS COMPILE
# Create app directory
WORKDIR /usr/src/app
# Copy project to working dir
COPY samplewebapp /usr/src/app
# Compile first (package)
RUN mvn clean compile

# Then package
FROM maven:3.5-jdk-8-alpine AS PACKAGE
# Create app directory
WORKDIR /usr/src/app
# Copy project to working dir
COPY samplewebapp /usr/src/app
# Compile first (package)
RUN mvn package

# Do not run anything for now
